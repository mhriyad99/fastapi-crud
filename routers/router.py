from fastapi import APIRouter

from routers import blog, user, authentication

api_router = APIRouter()

api_router.include_router(authentication.router)
api_router.include_router(blog.router)
api_router.include_router(user.router)
