from typing import List

from pydantic import BaseModel


class BlogBase(BaseModel):
    title: str
    body: str
    creator_id: int


class Blog(BaseModel):
    title: str
    body: str

    class Config:
        orm_mode = True


class CreateBlogShow(BaseModel):
    title: str
    body: str

    class Config:
        orm_mode = True


class Show(BaseModel):
    name: str
    email: str


class User(BaseModel):
    name: str
    email: str
    password: str


class ShowUser(BaseModel):
    name: str
    email: str
    blog: List[Blog] = []

    class Config:
        orm_mode = True


class ShowAuthor(BaseModel):
    name: str
    email: str

    class Config:
        orm_mode = True


class ShowBlog(BaseModel):
    title: str
    body: str
    creator: ShowAuthor

    class Config:
        orm_mode = True


class Login(BaseModel):
    email: str;
    password: str;


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str | None = None
