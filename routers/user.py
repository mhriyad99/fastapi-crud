from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session
import oauth2
import schemas
from database import get_db
from repository import user

router = APIRouter(
    prefix='/user',
    tags=["user-router"]
)


@router.post('/', status_code=status.HTTP_201_CREATED, response_model=schemas.Show)
def create_user(request: schemas.User, db: Session = Depends(get_db),
                current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.create_user(request, db)


@router.get('/{id}', response_model=schemas.ShowUser)
def get_user(id: int, db: Session = Depends(get_db),
             current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.show(id, db)
