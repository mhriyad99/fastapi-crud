### setup environment
first install python from official site
install virtualenv from pypi

### create venv
```shell
python -m venv env
```

### activate venv
for windows
```shell
env\Scripts\activate
```
for linux
```shell
source env/bin/activate
```
### install packages
```shell
pip install -r requirements.txt
```

### run server
```shell
uvicorn main:app --port 8000 --reload
```