from fastapi import FastAPI

import models
from database import engine
from routers import router

app = FastAPI()

models.Base.metadata.create_all(bind=engine) # Create all the tables defined in the models.py

app.include_router(router.api_router)
